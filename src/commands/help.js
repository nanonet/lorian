const { MessageEmbed } = require('discord.js');
const config = require('../../config/main');

exports.run = (client, msg, args) => {
    msg.delete();
    let help = '';
    client.commands.forEach(element => {
        if(element.help.enabled) {
            help = help + `\n**${element.help.name}**: \`${element.help.usage}\` - ${element.help.description}`
        }
    });

    const embed = new MessageEmbed()
        .setAuthor("Bot Commands", "","")
        .setColor(0x6688ff)
        .setDescription(help)
        .setFooter("Powered by a small hamster")
        .setTimestamp()
    msg.channel.send(embed);
};

exports.help = {
    enabled: true,
    name: 'help',
    usage: `${config.prefix}help`,
    description: 'Lists all commands'
};