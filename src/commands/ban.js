const { MessageEmbed } = require('discord.js');
const config = require('../../config/main');
exports.run = (client, msg, args) => {
    const user = msg.guild.member(msg.author)
    if(user.hasPermission('BAN_MEMBERS')) {
        const user = msg.mentions.users.first();
        if(user) {
            const member = msg.guild.member(user);

            if(member) {
                member
                    .ban({days: 7, reason: `Banned by ${msg.author.username}`})
                    .then(() => {
                        const embed = new MessageEmbed()
                            .setAuthor(msg.author.username)
                            .setColor(0x66ff88)
                            .setDescription(`${member.user.tag} Has been banned!`)
                            .setFooter("Powered by a small hamster")
                            .setTimestamp()
                        msg.channel.send(embed);
                    }).catch(err => {
                        const embed = new MessageEmbed()
                            .setAuthor(msg.author.username)
                            .setColor(0xff8866)
                            .setDescription("I was unable to ban the member")
                            .setFooter("Powered by a small hamster")
                            .setTimestamp()
                        msg.channel.send(embed);
                        console.log(err);
                    });
            } else {
                const embed = new MessageEmbed()
                    .setAuthor(msg.author.username)
                    .setColor(0xff8866)
                    .setDescription("That user isn't in this guild!")
                    .setFooter("Powered by a small hamster")
                    .setTimestamp()
                msg.channel.send(embed);
            }
        } else if (args[0] != null) {
            const user = client.users.fetch(args[0]);
            Promise.resolve(user).then(function(user) {
                const member = msg.guild.member(user);
                member
                    .ban({days:7, reason:`banned by ${msg.author.username}`})
                    .then(() => {
                        const embed = new MessageEmbed()
                        .setAuthor(msg.author.username)
                        .setColor(0x66ff88)
                        .setDescription(`${member.user.tag} Has been banned!`)
                        .setFooter("Powered by a small hamster")
                        .setTimestamp()
                        msg.channel.send(embed);
                    }).catch(err =>{
                    const embed = new MessageEmbed()
                        .setAuthor(msg.author.username)
                        .setColor(0xff8866)
                        .setDescription("I was unable to ban the member")
                        .setFooter("Powered by a small hamster")
                        .setTimestamp()
                    msg.channel.send(embed);
                    console.log(err);
                    })
            });
        } else {
            const embed = new MessageEmbed()
                .setAuthor(msg.author.username)
                .setColor(0xff8866)
                .setDescription("You didn't mention the user to ban!")
                .setFooter("Powered by a small hamster")
                .setTimestamp()
            msg.channel.send(embed);
        }
    } else {
        const embed = new MessageEmbed()
            .setAuthor(msg.author.username)
            .setColor(0xff8866)
            .setDescription("You need the `BAN_MEMBERS` permission to use this")
            .setFooter("Powered by a small hamster")
            .setTimestamp()
        msg.channel.send(embed);
    }
    msg.delete()
};

exports.help = {
    enabled: true,
    name: 'ban',
    usage: `${config.prefix}ban @user`,
    description: 'Ban a user'
};