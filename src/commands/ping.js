const config = require('../../config/main');

exports.run = (client, msg, args) => {
    msg.channel.send(':watch: | Ping!').then(m => {
        m.edit(`:watch: | Pong! \`${m.createdTimestamp - msg.createdTimestamp}ms\``);
    });
    msg.delete()
};

exports.help = {
    enabled: true,
    name: 'ping',
    usage: config.prefix + 'ping',
    description: 'Pings the bot to check its connection speed.'
};