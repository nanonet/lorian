const { MessageEmbed } = require('discord.js');
const config = require('../../config/main');
const fs = require('fs');

exports.run = (client, msg, args) => {
    const user = msg.guild.member(msg.author)
    if(user.hasPermission('MANAGE_MESSAGES')) {
        const user = msg.mentions.users.first();
        if(user) {
            const member = msg.guild.member(user);

            if(member) {
                const mutedRole = msg.guild.roles.cache.find(mR => mR.name === 'Muted');
                if(!mutedRole) {
                    const embed = new MessageEmbed()
                        .setAuthor(msg.author.username)
                        .setColor(0xff8866)
                        .setDescription("Muted role does not exist")
                        .setFooter("Powered by a small hamster")
                        .setTimestamp()
                    msg.channel.send(embed);
                } else {
                    if(!member.roles.cache.has(mutedRole.id)) {
                        const embed = new MessageEmbed()
                            .setAuthor(msg.author.username)
                            .setColor(0xff8866)
                            .setDescription("This user is not muted")
                            .setFooter("Powered by a small hamster")
                            .setTimestamp()
                        msg.channel.send(embed);
                    } else {
                        delete client.muted[member.id]

                        member.roles.remove(mutedRole);

                        fs.writeFile('config/muted.json', JSON.stringify(client.muted, null, 4), err => {
                            if (err) console.log(err);
                            const embed = new MessageEmbed()
                                .setAuthor(msg.author.username)
                                .setColor(0x66ff88)
                                .setDescription(`${member.user.tag} Has been unmuted!`)
                                .setFooter("Powered by a small hamster")
                                .setTimestamp()
                            msg.channel.send(embed);
                        });
                    }
                }
            } else {
                const embed = new MessageEmbed()
                    .setAuthor(msg.author.username)
                    .setColor(0xff8866)
                    .setDescription("That user isn't in this guild!")
                    .setFooter("Powered by a small hamster")
                    .setTimestamp()
                msg.channel.send(embed);
            }
        } else {
            const embed = new MessageEmbed()
                .setAuthor(msg.author.username)
                .setColor(0xff8866)
                .setDescription("You didn't mention the user to unmute!")
                .setFooter("Powered by a small hamster")
                .setTimestamp()
            msg.channel.send(embed);
        }
    } else {
        const embed = new MessageEmbed()
            .setAuthor(msg.author.username)
            .setColor(0xff8866)
            .setDescription("You need the `MANAGE_MESSAGES` permission to use this")
            .setFooter("Powered by a small hamster")
            .setTimestamp()
        msg.channel.send(embed);
    }
    msg.delete()
};

exports.help = {
    enabled: true,
    name: 'unmute',
    usage: `${config.prefix}unmute @user`,
    description: 'Unmute a user'
};