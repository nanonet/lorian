const { MessageEmbed } = require('discord.js');
const config = require('../../config/main');
const fs = require('fs');

const owners = config.owner
exports.run = (client, msg, args) =>{
    if(owners.includes(msg.author.id)) {
        let option = args[0];
        let user = args[1];
        if (option === 'add') {
            client.blacklist[user] = {
                time: Date.now()
            };

            fs.writeFile('config/blacklisted.json', JSON.stringify(client.blacklist, null, 4), err => {
                if (err) console.log(err);
                const embed = new MessageEmbed()
                    .setAuthor(msg.author.username)
                    .setColor(0x66ff88)
                    .setDescription(`<@${user}> Has been blacklisted!`)
                    .setFooter("Powered by a small hamster")
                    .setTimestamp()
                msg.channel.send(embed);
            });
        } else if (option === 'del') {
            if (client.blacklist.hasOwnProperty(user)) {
                delete client.blacklist[user]

                fs.writeFile('config/blacklisted.json',JSON.stringify(client.blacklist, null,4), err => {
                    if (err) console.log(err);
                    const embed = new MessageEmbed()
                        .setAuthor(msg.author.username)
                        .setColor(0x66ff88)
                        .setDescription(`<@${user}> Had been unblacklisted`)
                        .setFooter("Powered by a small hamster")
                        .setTimestamp()
                    msg.channel.send(embed);
                });
            } else {
                const embed = new MessageEmbed()
                        .setAuthor(msg.author.username)
                        .setColor(0xff8866)
                        .setDescription(`User is not blacklisted.`)
                        .setFooter("Powered by a small hamster")
                        .setTimestamp()
                    msg.channel.send(embed);
            }
        } else {
            msg.channel.send('You must provide add or del!');
        }
    } else {
        msg.channel.send('You are not allowed to use the command.');
    }
};
exports.help = {
    enabled: false,
    name: 'blacklist',
    usage: `${config.prefix}blacklist <add/del> user id`,
    description: 'blacklist a user from the bot'
};