const fs = require('fs');
const path = require('path');
const config = require('../config/main');
const Discord = require('discord.js');
const client = new Discord.Client({ disableEveryone: true });
const commands = new Map();
client.commands = commands;
client.muted = require('../config/muted.json');
client.blacklist = require('../config/blacklisted.json');
let discordInvite = /(https:\/\/)?(www\.)?(discord\.gg|discord\.me|discordapp\.com\/invite|discord\.com\/invite)\/([a-z0-9-.]+)?/i;

client.on('ready', () => {
    console.log('started');
});


fs.readdirSync(path.resolve(__dirname, 'commands'))
    .filter(f => f.endsWith('.js'))
    .forEach(f => {
    console.log(`Loading command ${f}`);
    try {
        let command = require(`./commands/${f}`);
        if (typeof command.run !== 'function') {
            throw 'Command is missing a run function!';
        } else if (!command.help || !command.help.name) {
            throw 'Command is missing a valid help object!';
        }
        commands.set(command.help.name, command);
    } catch (error) {
        console.error(`Failed to load command ${f}: ${error}`);
    }
});

client.on('message', message => {
    if (message.author.bot || !message.guild) {
        return;
    }
    if (client.blacklist.hasOwnProperty(message.author.id)) {
        return;
    }
    
    if (discordInvite.test(message.content)) {
        message.channel.send(`${message.author}, you are not allowed to send discord invites.`);
        message.delete();
    }

    let { content } = message;

    let prefix = config.prefix;
    if (!content.startsWith(prefix)) {
        return;
    }
    let split = content.substr(prefix.length).split(' ');
    let label = split[0];
    let args = split.slice(1);
        
    if (commands.get(label)) {
    commands.get(label).run(client, message, args);
    }
});
client.on('messageUpdate', (oldMessage, newMessage) => {
    if (discordInvite.test(newMessage.content)) {
        newMessage.channel.send(`${newMessage.author}, you are not allowed to send discord invites.`)
        newMessage.delete()
        return;
    }
});

client.login(config.token);